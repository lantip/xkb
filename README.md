# xkb

Untuk menguji :

Salin berkas jv ke :


`/usr/share/X11/xkb/symbols`

salin kode dibawah ini
```
    <layout>
      <configItem>
        <name>jv</name>
        <shortDescription>jv</shortDescription>
        <description>Indonesian (Javanese Pegon)</description>
        <languageList>
          <iso639Id>jv</iso639Id>
        </languageList>
      </configItem>
      <variantList/>
    </layout>
```
Letakkan pada berkas `/usr/share/X11/xkb/rules/evdev.xml` pada bagian bawah

```
    <layout>
      <configItem>
        <name>id</name>
        <shortDescription>id</shortDescription>
        <description>Indonesian (Jawi)</description>
        <languageList>
          <iso639Id>ind</iso639Id>
          <iso639Id>jv</iso639Id>
          <iso639Id>msa</iso639Id>
          <iso639Id>min</iso639Id>
          <iso639Id>ace</iso639Id>
          <iso639Id>bjn</iso639Id>
          <iso639Id>tsg</iso639Id>
          <iso639Id>mfa</iso639Id>
        </languageList>
      </configItem>
      <variantList/>
    </layout>
```

Jika sudah, coba ke setting gnome control center, tambahkan, selanjutnya reboot komputer Anda


